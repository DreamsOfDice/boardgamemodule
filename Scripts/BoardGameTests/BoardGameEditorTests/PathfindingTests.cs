﻿using BoardGameFrameworkCore;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameTests
{
    public class PathfindingTests
    {
        Board boardTest;
        PathfindingControler newPathfinder;

        [SetUp]
        public void Setup()
        {

            boardTest = new Board(2, 3);
            newPathfinder = new PathfindingControler();

        }

        [Test]
        public void FindSimplePathTest()
        {
            BoardSpace initialSpace = boardTest.GetSpace(1, 0);
            BoardSpace endSpace = boardTest.GetSpace(0,2);

            List<BoardSpace> path = newPathfinder.GeneratePath(boardTest, initialSpace, endSpace,5);

            Assert.AreEqual(3,path.Count);


            Board boardTest1 = new Board(2, 3);

            BoardSpace markSpace = boardTest1.GetSpace(0, 1);
            initialSpace = boardTest1.GetSpace(1, 0);
            endSpace = boardTest1.GetSpace(0, 2);

            markSpace.SetPassableStatus(false);

            path = newPathfinder.GeneratePath(boardTest1, initialSpace, endSpace, 5);

            foreach (BoardSpace pathSpace in path) {
                Assert.AreNotEqual(boardTest.GetSpace(0, 1), pathSpace);
            }
            
        }

        [Test]
        public void FindMostEfficientPathTest()
        {
            Board boardTest1 = new Board(3, 6);

            BoardSpace initialSpace = boardTest1.GetSpace(0, 0);
            BoardSpace endSpace = boardTest1.GetSpace(2, 5);

            BoardSpace markSpace = boardTest1.GetSpace(0, 2);
            markSpace.SetPassableStatus(false);

            markSpace = boardTest1.GetSpace(2, 4);
            markSpace.SetPassableStatus(false);

            List<BoardSpace> path = newPathfinder.GeneratePath(boardTest1, initialSpace, endSpace, 5);

            Assert.AreEqual(7, path.Count);

            foreach (BoardSpace pathSpace in path)
            {
                Assert.AreNotEqual(boardTest1.GetSpace(0, 2), pathSpace);
                Assert.AreNotEqual(boardTest1.GetSpace(2, 4), pathSpace);

            }

        }

        [Test]
        public void DestinationIsTheSameAsOriginTest()
        {
            BoardSpace initialSpace = boardTest.GetSpace(1, 0);
            BoardSpace endSpace = boardTest.GetSpace(1, 0);

            List<BoardSpace> path = newPathfinder.GeneratePath(boardTest, initialSpace, endSpace, 5);

            Assert.IsNull(path);
            
        }

        [Test]
        public void PathNotPossibleTest()
        {
            BoardSpace initialSpace = boardTest.GetSpace(1, 0);
            BoardSpace endSpace = boardTest.GetSpace(0, 2);

            BoardSpace markSpace = boardTest.GetSpace(1, 1);
            markSpace.SetPassableStatus(false);
            markSpace = boardTest.GetSpace(0, 1);
            markSpace.SetPassableStatus(false);

            List<BoardSpace> path = newPathfinder.GeneratePath(boardTest, initialSpace, endSpace, 5);

            Assert.IsNull(path);


        }



    }
}
