﻿using BoardGameFrameworkCore;
using BoardGameFrameworkUI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BoardGameFrameworkEditor
{
    
    public class LevelDesigner : MonoBehaviour
    {
        [BoardGameFrameworkHelpBox("You must ensure the values of row and columns are not zero, otherwise the generate board button does nothing")]
        public int rows;
        public int columns;

        [BoardGameFrameworkHelpBox("You need to select the movement type, be it eight or four direction. If none is selected, default is four")]
        public BoardType typeOfBoardMovement;

        [BoardGameFrameworkHelpBox("You must have some kind of space prefab object, even if its just a an empty object, to create a visible board")]
        public SpriteRenderer SpaceTemplate;

        List<RowsHelperFunction> allSpaces = new List<RowsHelperFunction>();

        [BoardGameFrameworkHelpBox("Once generated, the gameboard has rows and inside each row is the columns. Each space in here is a cell which you may customize or edit at will")]
        public List<BoardSpaceUI> boardUISpaces = new List<BoardSpaceUI>();

        protected Board gameBoard;

        void Awake()
        {
            SetupGame();   
        }

        public virtual bool SetupGame()
        {
            if (rows != 0 && columns != 0)
            {

                List<RowsHelperFunction> spacesToInstantiate = new List<RowsHelperFunction>();

                for (int i = 0; i<rows; i++) {
                    spacesToInstantiate.Add(new RowsHelperFunction());
                }

                foreach (BoardSpaceUI graphicalSpace in boardUISpaces) {
                    spacesToInstantiate[graphicalSpace.spaceControler.GetRowLocationInBoard()].AddColumnSpace(graphicalSpace.spaceControler);
                }

                BoardData newData = new BoardData(typeOfBoardMovement,spacesToInstantiate);
                gameBoard = new Board(newData);

                foreach (BoardSpaceUI graphicalSpace in boardUISpaces)
                {
                    graphicalSpace.clickedEventHandler += gameBoard.SpaceClickedIntermidiary;

                    graphicalSpace.hoverBeginEventHandler += space=> graphicalSpace.GetComponent<SpriteRenderer>().sprite = space.GetImage();
                    graphicalSpace.hoverEndEventHandler += space => graphicalSpace.GetComponent<SpriteRenderer>().sprite = SpaceTemplate.sprite;


                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public Board GetBoard()
        {
            return gameBoard;
        }

        void OnValidate() {
           
        }

        public void GenerateBoard()
        {
            if (SpaceTemplate == null) {
                throw new Exception("You must assign a space template");
            }

            if (rows != 0 && columns != 0)
            {                
                SetUpGraphics();
                
            }
            else
            {
                throw new Exception("You must assign a columns and spaces other than zero");
            }
        }

        protected virtual void SetUpGraphics()
        {
            boardUISpaces = new List<BoardSpaceUI>();
            GameObject boardHolder;
            if (transform.childCount ==0)
            {
                boardHolder = new GameObject();
                boardHolder.transform.SetParent(transform);

            }
            else {
                boardHolder = transform.GetChild(0).gameObject;
                DestroyImmediate(boardHolder);
                boardHolder = new GameObject();
                boardHolder.transform.SetParent(transform);
            
            }

            for (int i = 0; i<rows;i++) {
                for (int j = 0; j<columns;j++) {
                    Vector2 sizeOfCellDefault= SpaceTemplate.GetComponent<SpriteRenderer>().size;

                    GameObject spaceInWorld = Instantiate(SpaceTemplate.gameObject, new Vector3(0+ sizeOfCellDefault.x*j, 0-(i* sizeOfCellDefault.y), 0), Quaternion.identity, boardHolder.transform);
                    //spaceInWorld.AddComponent<SpriteRenderer>().sprite = SpaceTemplate.sprite;
                    BoardSpaceData newSpaceData = new BoardSpaceData(i,j);

                    spaceInWorld.AddComponent<BoardSpaceUI>();
                    spaceInWorld.GetComponent<BoardSpaceUI>().spaceControler = new BoardSpace(newSpaceData);
                    spaceInWorld.GetComponent<BoardSpaceUI>().spaceControler.SetBoardUIReference(spaceInWorld.GetComponent<BoardSpaceUI>());
                    boardUISpaces.Add(spaceInWorld.GetComponent<BoardSpaceUI>());

                }
               
                  
                }
            }

           

        }

    }

