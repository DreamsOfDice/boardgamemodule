﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFrameworkEditor
{
    public class BoardGameFrameworkHelpBoxAttribute : PropertyAttribute
    {
        public string text;

        public BoardGameFrameworkHelpBoxAttribute(string newText) {
            text = newText;
        }

    }
}