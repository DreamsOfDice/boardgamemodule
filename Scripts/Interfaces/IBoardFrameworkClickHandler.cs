﻿using BoardGameFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardFrameworkInterfaces
{
    public interface IBoardFrameworkClickHandler 
    {
        void PointerClickHandler(BoardSpace spaceClicked);
        void PointerDoubleClickHandler(BoardSpace spaceDoubleClicked);
    }
}