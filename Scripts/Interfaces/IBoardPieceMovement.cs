﻿using BoardGameFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFrameworkUI
{
    public interface IBoardPieceMovement
    {
        bool MovementHandling(BoardPiece ObjectToMove);
        void SetNewPath(List<BoardSpace> newPath);
        bool IsPathEmpty();
    }
}