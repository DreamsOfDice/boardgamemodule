﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BoardGameFrameworkCore
{
    [Serializable]
    public class Board
    {
        BoardData data;

        public delegate void BoardSpaceClickedEvent(BoardSpace spaceClicked);
        public BoardSpaceClickedEvent boardSpaceClickedEventHandler;

        public Board(BoardData newData) {

            data = newData;
            SetUpBoardSpaceNeighbours();

        }

        public Board(int rows, int columns, BoardSpaceData baseSpace = null) {
            if (rows <= 0 || columns <= 0)
            {
                throw new Exception("Invalid dimensions. A Board must have a positive amount of both columns and rows. ");
            }

            List<RowsHelperFunction> boardSpaces = GenerateBoard(rows,columns,baseSpace);


            data = new BoardData(boardSpaces);
            SetUpBoardSpaceNeighbours();

        }

        public Board(int rows, int columns, BoardType movementType, BoardSpaceData baseSpace = null) {
            if (rows <= 0 || columns <= 0)
            {
                throw new Exception("Invalid dimensions. A Board must have a positive amount of both columns and rows. ");
            }

            List<RowsHelperFunction> boardSpaces = GenerateBoard(rows, columns, baseSpace);
            data = new BoardData(movementType,boardSpaces);
            SetUpBoardSpaceNeighbours();
        }

        public void SpaceClickedIntermidiary(BoardSpace spaceClicked) {
            boardSpaceClickedEventHandler.Invoke(spaceClicked);
        }

        protected virtual List<RowsHelperFunction> GenerateBoard(int rows, int columns, BoardSpaceData defualtSpaceData)
        {
            List<RowsHelperFunction> boardSpaces = new List<RowsHelperFunction>();

            for (int i = 0; i < rows; i++)
            {
                List<BoardSpace> boardSpacesColumnsInRow = new List<BoardSpace>();
                for (int j = 0; j < columns; j++)
                {
                    if (defualtSpaceData == null)
                    {
                        boardSpacesColumnsInRow.Add(new BoardSpace(new BoardSpaceData(i, j)));
                    }
                    else
                    {
                        boardSpacesColumnsInRow.Add(new BoardSpace(defualtSpaceData, i, j));
                    }
                }
                boardSpaces.Add(new RowsHelperFunction(boardSpacesColumnsInRow));
                

            }
            return boardSpaces;
        }
        
        public bool SetUpBoardSpaceNeighbours() {
            if (data.GetBoardSpaces().Count == 0) {
                return false;
            }

            List<RowsHelperFunction> boardSpaces = data.GetBoardSpaces();
            if (data.GetBoardMovementType() == BoardType.FOUR_WAY_MOVEMENT)
            {
                SetupFourWayNeighbours(boardSpaces);

            }
            else {
                SetupEightWayNeighbours(boardSpaces);
            }
          

            return true;
        }

        protected virtual void SetupFourWayNeighbours(List<RowsHelperFunction> boardSpaces) {

            
            for (int x = 0; x < boardSpaces.Count; x++)
            {
                for (int y = 0; y < boardSpaces[x].GetColumns().Count; y++)
                {
                    List<BoardSpace> newNeighbours = new List<BoardSpace>();

                    // This is the 4-way connection version:
                    if (x > 0)
                        newNeighbours.Add(boardSpaces[x-1].GetColumns()[y]);
                    if (x < boardSpaces.Count - 1)
                        newNeighbours.Add(boardSpaces[x +1].GetColumns()[y]);
                    if (y > 0)
                        newNeighbours.Add(boardSpaces[x].GetColumns()[y-1]);
                    if (y < boardSpaces[x].GetColumns().Count-1)
                        newNeighbours.Add(boardSpaces[x].GetColumns()[y+1]);

                    boardSpaces[x].GetColumns()[y].SetNewNeighbours(newNeighbours);

                }

            }

        }

        protected virtual void SetupEightWayNeighbours(List<RowsHelperFunction> boardSpaces) {
            
                      for (int x = 0; x < boardSpaces.Count; x++)
                      {
                          for (int y = 0; y < boardSpaces[x].GetColumns().Count; y++)
                          {

                           List<BoardSpace> newNeighbours = new List<BoardSpace>();


                              // This is the 8-way connection version (allows diagonal movement)
                              // Try left
                           if (x > 0)
                              {
                                  newNeighbours.Add(boardSpaces[x - 1].GetColumns()[y]);
                                  if (y > 0)
                                    newNeighbours.Add(boardSpaces[x - 1].GetColumns()[y-1]);
                                  if (y < boardSpaces[x].GetColumns().Count - 1)
                                    newNeighbours.Add(boardSpaces[x - 1].GetColumns()[y+1]); 
                              }

                              // Try Right
                              if (x < boardSpaces.Count - 1)
                              {
                                 newNeighbours.Add(boardSpaces[x + 1].GetColumns()[y]);
                                   if (y > 0)
                                         newNeighbours.Add(boardSpaces[x + 1].GetColumns()[y-1]);
                                   if (y <  boardSpaces[x].GetColumns().Count - 1)
                                         newNeighbours.Add(boardSpaces[x + 1].GetColumns()[y+1]);
                    }

                              // Try straight up and down
                              if (y > 0)
                                   newNeighbours.Add(boardSpaces[x].GetColumns()[y - 1]);
                             if (y < boardSpaces[x].GetColumns().Count - 1)
                                   newNeighbours.Add(boardSpaces[x].GetColumns()[y + 1]);

                    boardSpaces[x].GetColumns()[y].SetNewNeighbours(newNeighbours);
                }
            }

        }

        #region Getters

        public BoardSpace GetSpace(int row, int column)
        {
            return data.GetSpecificSpace(row,column);
        }

        public int GetColumnsSize()
        {
            return data.GetColumnAmount();
        }

        public int GetRowsSize()
        {
            return data.GetRowAmount();
        }

        public int GetBoardSize()
        {
            int rowsToCount= data.GetBoardSpaces().Count;
            int columnsToCount = data.GetBoardSpaces()[0].GetColumns().Count;
            return rowsToCount * columnsToCount;
            /*TODO: add ability to count columns that differe in each row and exception handling*/
        }

        public List<BoardSpace> GetSpaceNeighbors(int row, int column)
        {
            BoardSpace space = GetSpace(row, column);
            return space.GetNeighbours();
        }

        public List<RowsHelperFunction> GetFullBoardSpaces() {
            return data.GetBoardSpaces();
        }

        
        #endregion

    }
}